(ns cs378hw3.core)
;searches though possible errors and prints the result
(defn Query []
  (print "there is an error")
  (if-some [ Endlessloop Baseclass] 
   if-some [ Fail Loop IllegalOps ]
   if-some [Results Data Mismatch Void ]
           (:error true false)
           (print error))) 
;checks to see if loop does not stop
(defn Endlessloop [ input, output ]
  (identical? input output)
  )
;checks to see if base class is called instead of derived
;class
(defn Baseclass [ base, derived ]
  ((:class base) false)
  ((:class derived) true)
  (instance? base))
;checks to if there is any output
(defn Fail [ output ]
  (instance? output))
;chekcs loop for output, if the function is called correctly,
;to make sure the input and output do not match, and that
;output matches the expected results
(defn Loop [ input, output, x, results ]
  cond-> []
  (fn? x)
  (false?(= input output))
  (instance? output)
  (true? (= output results))
  )
;checks item type and if it is using the correct operations
(defn IllegalOps [ string, number, collection]
  cond-> []
  (string? string)
  (instance? number
             (if number
               (or '+' '-' '*' '/')
               (or '=' '==' '<' '>' '<=' '>=')))
  (coll? collection)
  )
;checks to see if the result is incorrect, partially correct
; of if there are any results
(defn Results [ results, output ]
  (true? (= results output))
  (some? (= results output))
  (nil? results)
  )
;either returns the item types in different classes
; or checks to see if there is an item type returned
(defn Data [ type ]
  { :a type } { :b type }
  (if type 
  (diff a b)
  (instance? type))
  )
;checks to see if the item types match
(defn Mismatch [ type ]
 ((:item type)
         (true? (= type type))))
;checks to see if a void class returns only a null
(defn Void [ output ]
  (nil? output) 
  )
; Queries for each rule
(def queryLoopCheck
  [:?type]
  [?loop <- Loop (== ?type type)])
(def queryOutput
  [:?output]
  [?fail <- Fail (== ?output ouput)])
(def queryMismatch
  [:?type]
  [?mismatch <- Mismatch (== ?type type)])
(def queryEndLoop
  [:?output]
  [?endlessloop <- EndlessLoop (== ?output output)])
(def querybase
  [:?derived]
  [?baseclass <- Baseclass (== ?derived derived)])
(def queryIllegOps
  [:?type]
  [?illegalops <- IllegalOps (== ?type type)])
(def queryResults
  [:?output]
  [?results <- Results (== ?output output)])
(def queryData
  [:?type]
  [?data <- Data (== ?type type)])
(def queryVoid
  [:?output]
  [?void <- Void (== ?output output)])

  

 
  
  
